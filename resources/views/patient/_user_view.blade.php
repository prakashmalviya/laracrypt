@extends('layout.master')
@section('title', 'user')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3 class="m-0">User</h3>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$patient->name ?? ''}}" readonly>
                            @error('name')
                            <div class="text-danger"></div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$patient->email ?? ''}}" readonly>
                            @error('email')
                            <div class="text-danger"></div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{$patient->phone ?? ''}}"readonly>
                            @error('phone')
                            <div class="text-danger"></div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address" readonly>{{$patient->address ?? ''}}</textarea>
                            @error('address')
                            <div class="text-danger"></div>
                            @enderror
                        </div>
                        <a href="{{route('patient.index')}}" class="btn btn-sm btn-primary">Back</a>
                </div>
            </div>
        </div>
    </section>

@endsection
