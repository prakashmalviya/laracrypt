@extends('layout.master')
@section('title', 'Patient Management')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3 class="m-0">Patient Management</h3>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Patients</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <div class="row">
                                <div class="col-md-6 mt-1">
                                    <form method="get" action="{{route('patient.index')}}" class="float-left w-50 mr-2" id="search-from">
                                        <div class="input-group ">
                                            <input type="text" class="form-control form-control-sm " id="search-text"
                                                   placeholder="Search by Name, Email, Phone, Address"
                                                   name="search" value="{{request()->query('search', '')}}">
                                            <div class="input-group-append">
                                                <button type="submit" id="search-btn" class="btn btn-sm btn-default search-btn-submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                                <button type="reset" class="btn btn-sm btn-default search-reset">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                    Reset
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-6 d-flex justify-content-sm-end mt-1">
                                    <a href="{{route('patient.seeder')}}" class="btn btn-sm btn-outline-success mr-2">
                                        <i class="fa fa-database" aria-hidden="true"></i>
                                        Seeder
                                    </a>
                                    <a href="{{route('patient.create')}}" class="btn btn-sm btn-outline-success">
                                        <i class="fa fa-plus"></i> New
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap table-striped  table-bordered ">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($patients) > 0)
                                    @foreach($patients as $patient)
                                        <tr>
                                            <td>{{$patient->name}}</td>
                                            <td>{{$patient->email}}</td>
                                            <td>{{$patient->phone}}</td>
                                            <td><span class="tag tag-success">{{$patient->address}}</span></td>
                                            <td class="text-center">
                                                <a class="btn btn-primary btn-sm mr-2 "
                                                   href="{{route('patient.show',$patient->id)}}">
                                                    <i class="fas fa-folder"></i>
                                                    View
                                                </a>
                                                <a class="btn btn-info btn-sm mr-2"
                                                   href="{{route('patient.edit',$patient->id)}}">
                                                    <i class="fas fa-pencil-alt"></i>
                                                    Edit
                                                </a>
                                                <a class="btn btn-danger btn-sm"
                                                   href="" data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal{{$patient->id}}">
                                                    <i class="fas fa-trash"></i>
                                                    Delete
                                                </a>
                                                <form action="{{route('patient.destroy',$patient->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$patient->id}}">
                                                        <div class="modal-dialog ">
                                                            <div class="modal-content">
                                                                <div class="modal-header bg-danger">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Are
                                                                        you
                                                                        sure to delete this item ?</h5>
                                                                    <button type="button" class="close"
                                                                            data-bs-dismiss="modal"> ×
                                                                    </button>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-sm btn-outline-secondary "
                                                                            data-bs-dismiss="modal">cancel
                                                                    </button>
                                                                    <button class="btn btn-sm btn-outline-danger ">
                                                                        Delete
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">
                                            <p class="text-center text-danger">There is no data found, please seed it for testing.</p>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-end mr-4 mt-3">
                                {{ $patients->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customJs')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.search-reset').click(function () {
                $('#search-text').val('');
                $('.search-btn-submit').click();
            });
        });
    </script>
@endsection
