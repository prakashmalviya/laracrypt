<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title',env('app_name'))</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/font-awesome-4.7.0/css/font-awesome.min.css')}}">


    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
{{--    sweat  alter--}}
{{--    <link rel="stylesheet" href="{{asset('adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">--}}

<!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
{{--<!-- DataTables -->--}}
{{--    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">--}}
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.1/dist/sweetalert2.min.css" rel="stylesheet">

    {{--toastr--}}
    <link rel="stylesheet" href="{{asset('adminlte/plugins/toastr/toastr.min.css')}}">

    <meta name="csrf-token" content="{{csrf_token()}}">
    <!-- Css files-->
    <link rel="stylesheet" type="text/css" href="{{asset('adminlte/plugins/simple-image-cropper/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('adminlte/plugins/simple-image-cropper/css/style-example.css')}}"    />
    <link rel="stylesheet" type="text/css" href="{{asset('adminlte/plugins/simple-image-cropper/css/jquery.Jcrop.min.css')}}" />
    <!-- Js files-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="{{asset('adminlte/plugins/simple-image-cropper/scripts/jquery.Jcrop.js')}}"></script>
    <script type="text/javascript" src="{{asset('adminlte/plugins/simple-image-cropper/scripts/jquery.SimpleCropper.js')}}"></script>
</head>

