<!DOCTYPE html>
<html lang="en">
@include('layout.header')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('layout.navbar')
    @include('layout.sidebar')
    <div class="content-wrapper">
        @yield('content')
    </div>
    @include('layout.footer')
</div>
<div id="modal-placeholder"></div>
@include('layout.script')
@yield('costume_js')
</body>
</html>
