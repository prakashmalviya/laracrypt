<?php

use App\Http\Controllers\PatientController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index']);
Route::resources(['user' => UserController::class]);
Route::get('seed/data', [UserController::class, 'seedData'])->name('user.seeder');

Route::resources(['patient' => PatientController::class]);
Route::get('seed-data', [PatientController::class, 'seedData'])->name('patient.seeder');