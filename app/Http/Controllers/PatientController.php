<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Faker\Generator as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = \request('search', '');


        $patients = Patient::select(
            'id',
            DB::raw("AES_DECRYPT(FROM_BASE64(`name`), '" . env('APP_KEY') . "') as name"),
            DB::raw("AES_DECRYPT(FROM_BASE64(`email`), '" . env('APP_KEY') . "') as email"),
            DB::raw("AES_DECRYPT(FROM_BASE64(`phone`), '" . env('APP_KEY') . "') as phone"),
            DB::raw("AES_DECRYPT(FROM_BASE64(`address`), '" . env('APP_KEY') . "') as address")
        )->where(function ($q) use ($search) {
            if ($search)
            {
                $q->where(DB::raw("AES_DECRYPT(FROM_BASE64(`name`), '" . env('APP_KEY') . "')"), 'like', "%{$search}%");
                $q->orWhere(DB::raw("AES_DECRYPT(FROM_BASE64(`email`), '" . env('APP_KEY') . "')"), 'like', "%{$search}%");
                $q->orWhere(DB::raw("AES_DECRYPT(FROM_BASE64(`phone`), '" . env('APP_KEY') . "')"), 'like', "%{$search}%");
                $q->orWhere(DB::raw("AES_DECRYPT(FROM_BASE64(`address`), '" . env('APP_KEY') . "')"), 'like', "%{$search}%");
            }
        })->paginate();

        return view('patient.index')->with('patients', $patients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient._from');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required',
            'email'   => 'required|email|unique:users',
            'phone'   => 'required',
            'address' => 'required',
        ]);


        $input['name']    = DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $request->name) . "', '" . env('APP_KEY') . "'))");
        $input['email']   = DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $request->email) . "', '" . env('APP_KEY') . "'))");
        $input['phone']   = DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $request->phone) . "', '" . env('APP_KEY') . "'))");
        $input['address'] = DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $request->address) . "', '" . env('APP_KEY') . "'))");

        Patient::create($input);

        return redirect(route('patient.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::find($id);

        return view('patient._user_view', compact('patient', $patient));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::find($id);

        return view('patient._edit_user')->with('patient', $patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient          = Patient::find($id);
        $patient->name    = $request->name;
        $patient->email   = $request->email;
        $patient->phone   = $request->phone;
        $patient->address = $request->address;
        $patient->save();

        return redirect(route('patient.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrFail($id);
        $patient->delete();

        return redirect(route('patient.index'));
    }

    public function seedData(Faker $faker)
    {
        for ($i = 0; $i < 20; $i++)
        {
            Patient::create([
                'name'    => DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $faker->name()) . "', '" . env('APP_KEY') . "'))"),
                'email'   => DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $faker->email()) . "', '" . env('APP_KEY') . "'))"),
                'phone'   => DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $faker->phoneNumber()) . "', '" . env('APP_KEY') . "'))"),
                'address' => DB::raw("TO_BASE64(AES_ENCRYPT('" . str_replace("'", "", $faker->address()) . "', '" . env('APP_KEY') . "'))"),
            ]);
        }

        return redirect(route('patient.index'));
    }
}
