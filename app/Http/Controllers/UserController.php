<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Faker\Generator as Faker;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = \request('search', '');

        $users = User::where(function ($q) use ($search) {
            if ($search)
            {
                $q->whereEncrypted('name', 'LIKE', "%$search%");
                $q->orWhereEncrypted('email', 'LIKE', "%$search%");
                $q->orWhereEncrypted('phone', 'LIKE', "%$search%");
                $q->orWhereEncrypted('address', 'LIKE', "%$search%");
            }
        })->paginate();

        return view('user.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user._from');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required',
            'email'   => 'required|email|unique:users',
            'phone'   => 'required',
            'address' => 'required',
        ]);

        User::create($request->all());

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('user._user_view', compact('user', $user));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user._edit_user')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user          = User::find($id);
        $user->name    = $request->name;
        $user->email   = $request->email;
        $user->phone   = $request->phone;
        $user->address = $request->address;
        $user->save();

        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect(route('user.index'));
    }

    public function seedData(Faker $faker)
    {
        for ($i = 0; $i < 20; $i++)
        {
            User::create([
                'name'    => $faker->name(),
                'email'   => $faker->email(),
                'phone'   => $faker->phoneNumber(),
                'address' => $faker->address()
            ]);
        }

        return redirect(route('user.index'));
    }
}
