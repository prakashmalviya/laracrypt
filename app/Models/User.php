<?php

namespace App\Models;

use ESolution\DBEncryption\Traits\EncryptedAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    use EncryptedAttribute;

    protected $fillable = ['name', 'email', 'phone', 'address'];

    protected $encryptable = ['name', 'email', 'phone', 'address'];

}
